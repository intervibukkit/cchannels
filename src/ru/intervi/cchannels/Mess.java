package ru.intervi.cchannels;

import java.io.File;

import org.bukkit.ChatColor;

import ru.intervi.littleconfig.ConfigLoader;
import ru.intervi.littleconfig.utils.Utils;

public class Mess {
	protected Mess() {
		load();
	}
	
	public String nochatperm = "нет прав чтобы писать в этот канал";
	public String nohears = "никто не слышит";
	public String nopex = "нет плашина PEX";
	public String noperm = "нет прав чтобы писать в чат";
	public String onplayer = "эту команду можнов выполнить только из игры";
	public String use = "/ch канал";
	public String notfound = "нет такого канала";
	public String help[] = {"/cch reload - перезагрузить конфиг"};
	public String nooperm = "нет прав на это действие";
	public String ch = "%ch%: %type%, радиус: %rad%";
	public String types[] = {"глобальный", "внутримировой", "локальный"};
	public String list = "Доступные каналы чата:";
	public String reload = "конфиги перезагружены";
	public String join = "вы подключились к %ch%";
	public String on = "режим чтения всех каналов включен";
	public String off = "режим чтения всех каналов выключен";
	
	private String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	void load() {
		String sep = File.separator;
		String path = Utils.getFolderPath(this.getClass()) + sep + "CChannels";
		File dir = new File(path);
		if (!dir.isDirectory()) dir.mkdirs();
		path += sep + "mess.yml";
		File file = new File(path);
		if (!file.isFile()) Utils.saveFile(this.getClass().getResourceAsStream("/mess.yml"), file);
		
		ConfigLoader loader = null;
		try {
			loader = new ConfigLoader(file, false);
		} catch(Exception e) {e.printStackTrace();}
		if (loader == null) return;
		
		nochatperm = color(loader.getString("nochatperm"));
		nohears = color(loader.getString("nohears"));
		nopex = color(loader.getString("nopex"));
		noperm = color(loader.getString("noperm"));
		onplayer = color(loader.getString("onplayer"));
		use = color(loader.getString("use"));
		notfound = color(loader.getString("notfound"));
		help = loader.getStringArray("help");
		for (int i = 0; i < help.length; i++) help[i] = color(help[i]);
		nooperm = color(loader.getString("nooperm"));
		ch = color(loader.getString("ch"));
		types = loader.getStringArray("types");
		for (int i = 0; i < types.length; i++) types[i] = color(types[i]);
		list = color(loader.getString("list"));
		reload = color(loader.getString("reload"));
		join = color(loader.getString("join"));
		on = color(loader.getString("on"));
		off = color(loader.getString("off"));
	}
}