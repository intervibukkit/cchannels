package ru.intervi.cchannels;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Matcher;

import ru.intervi.cchannels.Config.Channel;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import ru.tehkode.permissions.PermissionUser;

import com.earth2me.essentials.Essentials;

public class Main extends JavaPlugin implements Listener {
	private Config conf = new Config();
	private Mess mess = new Mess();
	
	private volatile ConcurrentHashMap<String, ArrayList<UUID>> map = new ConcurrentHashMap<String, ArrayList<UUID>>(); //каналы с игроками
	private volatile ConcurrentHashMap<UUID, String> users = new ConcurrentHashMap<UUID, String>(); //чтобы знать у кого какой канал
	private volatile ArrayList<UUID> all = new ArrayList<UUID>(); //те, кто читают все каналы
	private Essentials ess = null;
	
	private Essentials getEssentials() { //получить Essentials
	    Plugin plugin = getServer().getPluginManager().getPlugin("Essentials");
	    if (plugin == null || !(plugin instanceof Essentials)) {
	        return null;
	    }
	    return (Essentials) plugin;
	}
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		for (Player p : Bukkit.getOnlinePlayers()) setCh(p.getUniqueId(), conf.def); //если есть игроки - всех в дефолтный\
		if (conf.ess) ess = getEssentials();
	}
	
	private void setCh(UUID uuid, String ch) { //сменить канал игроку
		if (map.containsKey(ch)) {
			ArrayList<UUID> list = map.get(ch);
			if (!list.contains(uuid)) list.add(uuid);
		} else {
			map.put(ch, new ArrayList<>(Arrays.asList(new UUID[] {uuid})));
		}
		if (users.containsKey(uuid)) {
			String c = users.get(uuid);
			if (c.equals(ch)) return;
			map.get(c).remove(uuid);
			users.replace(uuid, ch);
		} else users.put(uuid, ch);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public synchronized void onChat(AsyncPlayerChatEvent event) { //игрок написал в чат
		if (!conf.enable) return;
		String msg = event.getMessage();
		if (msg == null || msg.isEmpty() || msg.trim().isEmpty()) return;
		msg = Matcher.quoteReplacement(msg);
		Player player = event.getPlayer();
		Location loc1 = player.getLocation();
		UUID uuid = player.getUniqueId();
		String name = player.getName().toLowerCase();
		String ch = null;
		if (!users.containsKey(uuid)) { //если ему почему-то не назначен канал
			setCh(player.getUniqueId(), conf.def);
			ch = conf.def;
		} else ch = users.get(uuid);
		//начинается форматирование
		Channel c = conf.ch.get(ch);
		String mess = c.FORMAT;
		try {
			mess = mess.replaceAll("%ch%", ch);
			if (player.isOp()) { //выделение операторов
				String op = ChatColor.translateAlternateColorCodes('&', conf.op.replaceAll("%name%", player.getDisplayName()));
				mess = mess.replaceAll("%name%", op);
			} else mess = mess.replaceAll("%name%", player.getDisplayName());
			PluginManager pm = Bukkit.getServer().getPluginManager();
			if (pm.isPluginEnabled("PermissionsEx")) { //установка префикса и суффикса
				PermissionUser user = ((PermissionsEx) pm.getPlugin("PermissionsEx")).
						getPermissionsManager().getUser(player);
				String world = loc1.getWorld().getName();
				String prefix = user.getPrefix(world).trim();
				String suffix = user.getSuffix(world).trim();
				mess = mess.replaceAll("%prefix%", prefix).replaceAll("%suffix%", suffix);
			} else getLogger().warning("[CChannels] " + this.mess.nopex);
			mess = ChatColor.translateAlternateColorCodes('&', mess); //установка цветов
			if (player.hasPermission("cch.chat-color")) msg = ChatColor.translateAlternateColorCodes('&', msg);
			mess = mess.replaceAll("%mess%", msg);
		} catch(Exception e) {
			getLogger().warning("[EXCEPTION] mess: " + mess + "; msg: " + msg);
			e.printStackTrace();
		}
		if (event.isCancelled()) { //отмененные сообщения пишутся в консоль
			getLogger().info("[c] " + mess);
			return;
		}
		//рассылка
		if (player.hasPermission("cch.chat") && (player.hasPermission("cch.all") || player.hasPermission("cch.ch." + ch))) {
			boolean h = false;
			for (UUID u : map.get(ch)) { //рассылка по каналу
				Player send = Bukkit.getPlayer(u);
				if (send == null) {
					delUUID(u);
					continue;
				}
				if (!c.GLOBAL) { //проверки для локального и внутримирового чата
					Location loc2 = send.getLocation();
					if (c.WORLD && !loc1.getWorld().getName().equals(loc2.getWorld().getName())) return;
					if (c.RADIUS > 0 && !loc1.getWorld().getName().equals(loc2.getWorld().getName()) &&
							loc1.distance(loc2) > c.RADIUS) return;
				}
				//для корректной работы с Essentials
				if (ess.getUser(u)._getIgnoredPlayers().contains(name)) continue;
				send.sendMessage(mess);
				if (!uuid.equals(u)) h = true;
			}
			for (UUID u : all) Bukkit.getPlayer(u).sendMessage(mess); //рассылка наблюдателям
			if (!h) {
				getLogger().info("[h] " + mess);
				player.sendMessage(this.mess.nohears.replaceAll("%ch%", ch));
			} else getLogger().info(mess);
		} else { //запись в консоль, если нет прав
			getLogger().info("[p] " + mess);
			player.sendMessage(this.mess.nochatperm.replaceAll("%ch%", ch));
		}
		event.setCancelled(true);
	}
	
	private synchronized void delUUID(UUID uuid) {
		all.remove(uuid);
		String key = users.get(uuid);
		if (key == null) return;
		ArrayList<UUID> list = map.get(key);
		if (list == null) return;
		list.remove(uuid);
		map.replace(key, list);
		users.remove(uuid);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onQuit(PlayerQuitEvent event) { //правка списков при выходе игрока с сервера
		if (!conf.enable) return;
		delUUID(event.getPlayer().getUniqueId());
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onJoin(PlayerJoinEvent event) { //назначение дефолтного канала зашедшим
		if (!conf.enable) return;
		setCh(event.getPlayer().getUniqueId(), conf.def);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onCom(ServerCommandEvent event) { //красивый say
		if (!conf.enable || conf.say.equalsIgnoreCase("no")) return;
		String com = event.getCommand();
		if (com.length() >= 4 && com.substring(0, 3).equalsIgnoreCase("say")) {
			event.setCancelled(true);
			String msg = com.substring(3).trim();
			if (msg.isEmpty()) return;
			String mess = conf.say.replaceAll("%mess%", msg);
			mess = ChatColor.translateAlternateColorCodes('&', mess);
			Bukkit.broadcastMessage(mess);
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("ch") || label.equalsIgnoreCase("channel")) { //игровые команды
			if (!(sender instanceof Player)) {
				sender.sendMessage(mess.onplayer);
				return true;
			}
			if (sender.hasPermission("cch.chat")) {
				if (args.length >= 1) {
					String ch = args[0].toLowerCase();
					if (ch.equals("list")) { //список каналов
						ArrayList<String> list = new ArrayList<String>();
						list.add(mess.list);
						for (String name : conf.ch.keySet()) {
							if (sender.hasPermission("cch.ch." + name)) {
								Channel c = conf.ch.get(name);
								String add = new String(mess.ch);
								add = add.replaceAll("%ch%", name).replaceAll("%rad%", String.valueOf(c.RADIUS));
								String type = "null";
								if (c.GLOBAL) type = mess.types[0];
								else if (c.WORLD) type = mess.types[1];
								else if (c.RADIUS > 0) type = mess.types[2];
								add = add.replaceAll("%type%", type);
								list.add(add);
							}
						}
						sender.sendMessage(list.toArray(new String[list.size()]));
						return true;
					} else if (!conf.ch.containsKey(ch)) sender.sendMessage(mess.notfound); //если нет такого канала
					else if (sender.hasPermission("cch.ch." + ch)) { //смена канала
						UUID uuid = ((Player) sender).getUniqueId();
						if (!users.containsKey(uuid) || !users.get(uuid).equals(ch))
							setCh(uuid, ch);
						sender.sendMessage(mess.join.replaceAll("%ch%", ch));
					} else sender.sendMessage(mess.nochatperm.replaceAll("%ch%", ch));
				} else sender.sendMessage(mess.use);
			} else sender.sendMessage(mess.noperm);
			return true;
		} else if (label.equalsIgnoreCase("cch") || label.equalsIgnoreCase("cchannels")) { //команды управления плагином
			if (args.length >= 1) {
				switch(args[0].toLowerCase()) {
				case "reload": //перезагрузка конфига
					if (sender.hasPermission("cch.reload")) {
						conf.load();
						mess.load();
						if (conf.ess) ess = getEssentials(); else ess = null;
						sender.sendMessage(mess.reload);
					} else sender.sendMessage(mess.nooperm);
					break;
				case "help": //вывод справки
					sender.sendMessage(mess.help);
					break;
				case "on": //включить режим наблюдения за всеми каналами
					if (sender.hasPermission("cch.chat-all")) {
						all.add(((Player) sender).getUniqueId());
						sender.sendMessage(mess.on);
					} else sender.sendMessage(mess.nooperm);
					break;
				case "off": //выключить
					if (sender.hasPermission("cch.chat-all")) {
						all.remove(((Player) sender).getUniqueId());
						sender.sendMessage(mess.off);
					} else sender.sendMessage(mess.nooperm);
					break;
				default:
					sender.sendMessage(mess.help);
				}
			} else sender.sendMessage(mess.help);
			return true;
		} else return false;
	}
}