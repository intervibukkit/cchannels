package ru.intervi.cchannels;

import java.io.File;
import java.util.HashMap;

import ru.intervi.littleconfig.ConfigLoader;
import ru.intervi.littleconfig.utils.Utils;

public class Config {
	protected Config() {
		load();
	}
	
	public class Channel {
		protected Channel(String name, long radius, boolean world, String format, boolean global) {
			NAME = name;
			RADIUS = radius;
			WORLD = world;
			FORMAT = format;
			GLOBAL = global;
		}
		
		public final String NAME;
		public final long RADIUS;
		public final boolean WORLD;
		public final String FORMAT;
		public final boolean GLOBAL;
	}
	
	public boolean enable = true;
	public boolean ess = false;
	public String def = "global";
	public String op = "&n%name%";
	public String say = "no";
	public HashMap<String, Channel> ch = new HashMap<String, Channel>();
	
	void load() {
		String sep = File.separator;
		String path = Utils.getFolderPath(this.getClass()) + sep + "CChannels";
		File dir = new File(path);
		if (!dir.isDirectory()) dir.mkdirs();
		path += sep + "config.yml";
		File file = new File(path);
		if (!file.isFile()) Utils.saveFile(this.getClass().getResourceAsStream("/config.yml"), file);
		
		ConfigLoader loader = null;
		try {
			loader = new ConfigLoader(file, false);
		} catch(Exception e) {e.printStackTrace();}
		if (loader == null) return;
		
		enable = loader.getBoolean("enable");
		ess = loader.getBoolean("ess");
		def = loader.getString("def");
		op = loader.getString("op");
		say = loader.getString("say");
		
		String names[] = loader.getSectionNames();
		for (String name : names) {
			ConfigLoader c = loader.getSection(name);
			Channel put = new Channel(name, c.getLongInSection(name, "radius"), c.getBooleanInSection(name, "world"),
					c.getStringInSection(name, "format"), c.getBooleanInSection(name, "global"));
			ch.put(name, put);
		}
	}
}